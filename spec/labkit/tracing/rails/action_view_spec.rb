# frozen_string_literal: true

describe Labkit::Tracing::Rails::ActionView do
  let(:render_template_payload) do
    # https://guides.rubyonrails.org/active_support_instrumentation.html#render-template-action-view
    {
      identifier: "/Users/adam/projects/notifications/app/views/posts/index.html.erb",
      layout: "layouts/application",
    }
  end

  let(:render_partial_payload) do
    # https://guides.rubyonrails.org/active_support_instrumentation.html#render-partial-action-view
    {
      identifier: "/Users/adam/projects/notifications/app/views/posts/_form.html.erb",
    }
  end

  let(:render_collection_payload) do
    # https://guides.rubyonrails.org/active_support_instrumentation.html#render-collection-action-view
    {
      identifier: "/Users/adam/projects/notifications/app/views/posts/_post.html.erb",
      count: 3,
      cache_hits: 0,
    }
  end

  let(:html_template_payload) do
    {
      identifier: "html template",
      layout: "layouts/application",
    }
  end

  let(:text_template_payload) do
    {
      identifier: "text template",
      layout: "layouts/application",
    }
  end

  let(:inline_template_payload) do
    {
      identifier: "inline template",
      layout: "layouts/application",
    }
  end

  describe ".template_identifier" do
    context "when Rails is not loaded" do
      it "returns nil" do
        hide_const("Rails")
        expect(described_class.template_identifier(render_template_payload)).to be(nil)
        expect(described_class.template_identifier(render_partial_payload)).to be(nil)
        expect(described_class.template_identifier(render_collection_payload)).to be(nil)
        expect(described_class.template_identifier(html_template_payload)).to be(nil)
        expect(described_class.template_identifier(text_template_payload)).to be(nil)
        expect(described_class.template_identifier(inline_template_payload)).to be(nil)
      end
    end

    context "when Rails is loaded" do
      it "returns identifier relative to Rails root" do
        stub_const("Rails", double(:Rails, root: "/Users/adam/projects/notifications"))
        expect(described_class.template_identifier(render_template_payload)).to eql("app/views/posts/index.html.erb")
        expect(described_class.template_identifier(render_partial_payload)).to eql("app/views/posts/_form.html.erb")
        expect(described_class.template_identifier(render_collection_payload)).to eql("app/views/posts/_post.html.erb")
        expect(described_class.template_identifier(html_template_payload)).to eql("html template")
        expect(described_class.template_identifier(text_template_payload)).to eql("text template")
        expect(described_class.template_identifier(inline_template_payload)).to eql("inline template")
      end
    end

    context "when input payload is invalid" do
      it "returns nil" do
        hide_const("Rails")
        expect(described_class.template_identifier({ invalid: "template_payload" })).to be(nil)
        stub_const("Rails", double(:Rails, root: "/Users/adam/projects/notifications"))
        expect(described_class.template_identifier({ invalid: "template_payload" })).to be(nil)
      end
    end
  end
end
