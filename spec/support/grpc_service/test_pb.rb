# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: spec/support/grpc_service/test.proto

require "google/protobuf"

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("spec/support/grpc_service/test.proto", :syntax => :proto3) do
    add_message "labkit_test.Msg" do
      optional :name, :string, 1
      optional :error_code, :int32, 2
    end
  end
end

module LabkitTest
  Msg = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("labkit_test.Msg").msgclass
end
