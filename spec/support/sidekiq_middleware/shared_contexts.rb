# frozen_string_literal: true

shared_context "with sidekiq client middleware setup" do
  before do
    Sidekiq.client_middleware do |chain|
      chain.add described_class
    end
  end

  after do
    Sidekiq.client_middleware do |chain|
      chain.remove described_class
    end
  end

  around do |example|
    Sidekiq::Testing.fake! do
      example.run
    end
  end
end

shared_context "with sidekiq server middleware setup" do
  before do
    Sidekiq::Testing.server_middleware do |chain|
      chain.add described_class
    end
  end

  after do
    Sidekiq::Testing.server_middleware do |chain|
      chain.remove described_class
    end
  end

  around do |example|
    Sidekiq::Testing.inline! do
      example.run
    end
  end
end
